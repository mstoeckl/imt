#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>

#include <linux/videodev2.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

static void errno_exit(const char *s) {
  fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
  exit(EXIT_FAILURE);
}

static int xioctl(int fh, int request, void *arg) {
  int r;

  do {
    r = ioctl(fh, request, arg);
  } while (-1 == r && EINTR == errno);

  return r;
}

const char *window = "Display Window";

static void showImg(const cv::Mat &img) {
  cv::namedWindow(window, cv::WINDOW_AUTOSIZE);
  cv::imshow(window, img);
  cv::waitKey(0);
}

static void showImg(IplImage *frame) {

  cv::namedWindow(window, cv::WINDOW_AUTOSIZE);
  cvShowImage(window, frame);

  cv::waitKey(0);
}

static void alternateShow(const cv::Mat &i1, const cv::Mat &i2) {
  const char *window = "Display Window";
  cv::namedWindow(window, cv::WINDOW_AUTOSIZE);
  uint i = 0;
  while (true) {
    i = (i + 1) % 2;

    cv::imshow(window, i == 0 ? i1 : i2);

    cv::waitKey(0);
  }
}

static cv::Mat grabImageFromCamera() {
  CvCapture *capture = nullptr;
  for (int i = 0; i < 20; i++) {
    capture = cvCaptureFromCAM(i);
    std::cerr << "Trying capture #" << i << " with "
              << (capture == nullptr ? "No success" : "Awesomeness")
              << std::endl;
    if (capture) {
      std::cerr << "Found capture at " << i << std::endl;
      break;
    }
  }
  if (!capture) {
    std::cerr << "Capture is null" << std::endl;
    exit(1);
  }
  IplImage *frame;
  try {
    frame = cvQueryFrame(capture);
  } catch (cv::Exception e) {
    std::cerr << "Could not read image :-(" << std::endl;
    exit(1);
  }
  cv::Mat q(frame);
  cv::Mat s = q.clone();
  cvReleaseCapture(&capture);
  return s;
}

static void integrateGrayscale(const cv::Mat &img, int64_t &count,
                               int64_t &area) {
  int w = img.size().width;
  int h = img.size().height;
  area = w * h;

  count = 0;
  for (int i = 0; i < w; i++) {
    for (int k = 0; k < h; k++) {
      const uint8_t &val = img.at<uint8_t>(cv::Point(w, h));
      count += val;
    }
  }
}

cv::Mat process_image(void *p, size_t s) {
  std::cerr << "Got an image" << std::endl;
  FILE *f = fopen("/home/msto/foo.txt", "w");
  fwrite(p, 1, s, f);
  fclose(f);

  cv::Mat inp(240, 320, CV_8UC2, p);

  cv::Mat output;
  cvtColor(inp, output, CV_YUV2BGR_YUY2);

  const char *window = "Display Window";

  cv::imshow(window, output);
  cv::waitKey(0);
  usleep(50000);
  return output;
}

static cv::Mat getCameraImageRaw(const char *dev_name) {
  cv::Mat matrix;
  struct stat st;
  int fd;
  if (-1 == stat(dev_name, &st)) {
    fprintf(stderr, "Cannot identify '%s': %d, %s\n", dev_name, errno,
            strerror(errno));
    exit(EXIT_FAILURE);
  }

  if (!S_ISCHR(st.st_mode)) {
    fprintf(stderr, "%s is no device\n", dev_name);
    exit(EXIT_FAILURE);
  }

  fd = open(dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);

  if (-1 == fd) {
    fprintf(stderr, "Cannot open '%s': %d, %s\n", dev_name, errno,
            strerror(errno));
    exit(EXIT_FAILURE);
  }

  struct v4l2_capability cap;
  struct v4l2_cropcap cropcap;
  struct v4l2_crop crop;
  struct v4l2_format fmt;
  unsigned int min;

  if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &cap)) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s is no V4L2 device\n", dev_name);
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_QUERYCAP");
    }
  }

  if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
    fprintf(stderr, "%s is no video capture device\n", dev_name);
    exit(EXIT_FAILURE);
  }

  if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
    fprintf(stderr, "%s does not support streaming i/o\n", dev_name);
    exit(EXIT_FAILURE);
  }

  /* Select video input, video standard and tune here. */

  CLEAR(cropcap);

  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap)) {
    crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    crop.c = cropcap.defrect; /* reset to default */

    if (-1 == xioctl(fd, VIDIOC_S_CROP, &crop)) {
      switch (errno) {
      case EINVAL:
        /* Cropping not supported. */
        break;
      default:
        /* Errors ignored. */
        break;
      }
    }
  } else {
    /* Errors ignored. */
  }

  CLEAR(fmt);

  bool force_format = true;

  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (force_format) {
    fmt.fmt.pix.width = 320;
    fmt.fmt.pix.height = 240;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;

    if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt))
      errno_exit("VIDIOC_S_FMT");

    /* Note VIDIOC_S_FMT may change width and height. */
  } else {
    /* Preserve original settings as set by v4l2-ctl for example */
    if (-1 == xioctl(fd, VIDIOC_G_FMT, &fmt))
      errno_exit("VIDIOC_G_FMT");
  }

  /* Buggy driver paranoia. */
  min = fmt.fmt.pix.width * 2;
  if (fmt.fmt.pix.bytesperline < min)
    fmt.fmt.pix.bytesperline = min;
  min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
  if (fmt.fmt.pix.sizeimage < min)
    fmt.fmt.pix.sizeimage = min;

  struct v4l2_requestbuffers req;

  CLEAR(req);

  req.count = 4;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_USERPTR;

  if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s does not support "
                      "user pointer i/o\n",
              dev_name);
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_REQBUFS");
    }
  }

  struct buffer {
    void *start;
    size_t length;
  };
  struct buffer *buffers;
  unsigned int n_buffers;

  buffers = (struct buffer *)calloc(4, sizeof(*buffers));

  if (!buffers) {
    fprintf(stderr, "Out of memory\n");
    exit(EXIT_FAILURE);
  }

  for (n_buffers = 0; n_buffers < 4; ++n_buffers) {
    buffers[n_buffers].length = fmt.fmt.pix.sizeimage;
    buffers[n_buffers].start = malloc(fmt.fmt.pix.sizeimage);

    if (!buffers[n_buffers].start) {
      fprintf(stderr, "Out of memory\n");
      exit(EXIT_FAILURE);
    }
  }

  for (uint i = 0; i < n_buffers; ++i) {
    struct v4l2_buffer buf;

    CLEAR(buf);
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_USERPTR;
    buf.index = i;
    buf.m.userptr = (unsigned long)buffers[i].start;
    buf.length = buffers[i].length;

    if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
      errno_exit("VIDIOC_QBUF");
  }
  enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
    errno_exit("VIDIOC_STREAMON");

  // begin main loop
  struct v4l2_buffer buf;
  uint count = 30;
  uint i;
  while (count-- > 0) {
    for (;;) {
      fd_set fds;
      struct timeval tv;
      int r;

      FD_ZERO(&fds);
      FD_SET(fd, &fds);

      /* Timeout. */
      tv.tv_sec = 2;
      tv.tv_usec = 0;

      r = select(fd + 1, &fds, NULL, NULL, &tv);

      if (-1 == r) {
        if (EINTR == errno)
          continue;
        errno_exit("select");
      }

      if (0 == r) {
        fprintf(stderr, "select timeout\n");
        exit(EXIT_FAILURE);
      }

      CLEAR(buf);

      buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      buf.memory = V4L2_MEMORY_USERPTR;

      if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
        switch (errno) {
        case EAGAIN:
          continue;

        case EIO:
        /* Could ignore EIO, see spec. */

        /* fall through */

        default:
          errno_exit("VIDIOC_DQBUF");
        }
      }

      for (i = 0; i < n_buffers; ++i)
        if (buf.m.userptr == (unsigned long)buffers[i].start &&
            buf.length == buffers[i].length)
          break;

      assert(i < n_buffers);

      matrix = process_image((void *)buf.m.userptr, buf.bytesused);

      if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
        errno_exit("VIDIOC_QBUF");

      break;
      /* EAGAIN - continue select loop. */
    }
  }

  // Cleanup
  type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (-1 == xioctl(fd, VIDIOC_STREAMOFF, &type))
    errno_exit("VIDIOC_STREAMOFF");
  for (i = 0; i < n_buffers; ++i)
    free(buffers[i].start);
  if (-1 == close(fd))
    errno_exit("close");

  fd = -1;
  return matrix;
}

int main() {
  std::cout << "Hello World!" << std::endl;

  // Another approach: locate all continuous color blobs (i.e, dividing by
  // average is smooth).
  // One of them will be the red/blue we want....

  cv::namedWindow(window, cv::WINDOW_AUTOSIZE);
  cv::Mat x = getCameraImageRaw("/dev/video0");
  //        alternateShow(x, x);
  return 1;

  //  cv::Mat image = grabImageFromCamera();
  //  image = cv::imread("/home/msto/tote_chute_angle9.png",
  //  CV_LOAD_IMAGE_COLOR);

  //   cv::Mat output(image);
  //   cv::Mat bw(image.size(), CV_8UC1);
  //   cv::Mat lower = cv::Mat(image.size(), CV_8UC3, cv::Scalar(125, 50, 20));
  //   cv::Mat upper = cv::Mat(image.size(), CV_8UC3, cv::Scalar(175, 100, 50));
  //   cv::inRange(output, lower, upper, bw);
  //   blur(bw, bw, cv::Size(3, 3));
  //   // This identifies the shape.
  //   // Positioning can be done by angle (best fit line, slope estimation for
  //   pts)
  //   // And by L/R position of the center of the shape
  //   // Area of the shape is a proxy for volume
  //   int64_t count;
  //   int64_t area;
  //   integrateGrayscale(bw, count, area);
  //   std::cout << "Count: " << count << " Area: " << area << " Ratio: " <<
  //   count / area << std::endl;
  //
  //   const std::vector<int> params = {CV_IMWRITE_JPEG_QUALITY, 95};
  //   cv::imwrite("read.jpg", image, params);
  //  alternateShow(image, image);

  return 0;
}
